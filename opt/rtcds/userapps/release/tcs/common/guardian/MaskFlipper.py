#################
# Greg Grabeel
# 2018-05-10
#################
from guardian import GuardState
import time

nominal = 'ANNULUS'
request = nominal

#############################################
# Initialization

CentralFlipper = 'ACTFLIP1'
AnnulusFlipper = 'ACTFLIP2'
CentralStatus = 'FLIP1MON'
AnnulusStatus = 'FLIP2MON'


def ActuatorPulse(flipper):
    ezca[flipper] = 0
    time.sleep(0.25)
    ezca[flipper] = 1
    time.sleep(0.25)
    ezca[flipper] = 0
    time.sleep(0.25)

#############################################
# States


class INIT(GuardState):
    def main(self):
        if ezca[CentralStatus] == 1 and ezca[AnnulusStatus] == 1:
            return 'NO_MASK'
        elif ezca[CentralStatus] == 0 and ezca[AnnulusStatus] == 1:
            return 'CENTRAL'
        elif ezca[CentralStatus] == 1 and ezca[AnnulusStatus] == 0:
            return 'ANNULUS'
        elif ezca[CentralStatus] == 0 and ezca[AnnulusStatus] == 0:
            log('Both masks down, going to NO_MASK')
            return 'NO_MASK'
        else:
            log('Masks in unknown state')

    def run(self):
        # Notify because there is a problem if it got here
        notify('Masks in unknown state')
        # Should this be here?
        return True


class NO_MASK(GuardState):
    index = 20
    request = True

    def main(self):
        log('Attempting to be in No Mask condition')
        if ezca[CentralStatus] == 0 and ezca[AnnulusStatus] == 0:
            ActuatorPulse(CentralFlipper)
            ActuatorPulse(AnnulusFlipper)
            log('Oops, both flippers down')
        elif ezca[CentralStatus] == 0 and ezca[AnnulusStatus] == 1:
            ActuatorPulse(CentralFlipper)
            log('Raising Central Mask')
        elif ezca[CentralStatus] == 1 and ezca[AnnulusStatus] == 0:
            ActuatorPulse(AnnulusFlipper)
            log('Raising Annulus Mask')
        else:
            log('Already in correct state')
        # Give it time to actuate
        self.timer['FlipperTimer'] = 5

    def run(self):
        if self.timer['FlipperTimer']:
            if ezca[CentralStatus] == 0 or ezca[AnnulusStatus] == 0:
                notify('Flipper(s) not in correct state')
                return False
            else:
                return True


class CENTRAL(GuardState):
    index = 30
    request = True

    def main(self):
        log('Attempting to be in Central Mask condition')
        if ezca[CentralStatus] == 0 and ezca[AnnulusStatus] == 0:
            ActuatorPulse(AnnulusFlipper)
            log('Oops, both flippers down, Central same, Annulus up')
        elif ezca[CentralStatus] == 1 and ezca[AnnulusStatus] == 1:
            ActuatorPulse(CentralFlipper)
            log('Central down, Annulus same')
        elif ezca[CentralStatus] == 1 and ezca[AnnulusStatus] == 0:
            ActuatorPulse(CentralFlipper)
            ActuatorPulse(AnnulusFlipper)
            log('Central up, Annulus down')
        else:
            log('Already in correct state')
        # Give it time to actuate
        self.timer['FlipperTimer'] = 5

    def run(self):
        if self.timer['FlipperTimer']:
            if ezca[CentralStatus] == 0 and ezca[AnnulusStatus] == 1:
                return True
            else:
                notify('Flipper(s) not in correct state')
                return False


class ANNULUS(GuardState):
    index = 40
    request = True

    def main(self):
        log('Attempting to be in Annulus Mask condition')
        if ezca[CentralStatus] == 0 and ezca[AnnulusStatus] == 0:
            ActuatorPulse(CentralFlipper)
            log('Oops, both flippers down, Central up, Annulus same')
        elif ezca[CentralStatus] == 1 and ezca[AnnulusStatus] == 1:
            ActuatorPulse(AnnulusFlipper)
            log('Central same, Annulus down')
        elif ezca[CentralStatus] == 0 and ezca[AnnulusStatus] == 1:
            ActuatorPulse(AnnulusFlipper)
            ActuatorPulse(CentralFlipper)
            log('Central up, Annulus down')
        else:
            log('Already in correct state')
        # Give it time to actuate
        self.timer['FlipperTimer'] = 5

    def run(self):
        if self.timer['FlipperTimer']:
            if ezca[CentralStatus] == 1 and ezca[AnnulusStatus] == 0:
                return True
            else:
                notify('Flipper(s) not in correct state')
                return False


#############################################
# Edges

edges = [
    ('NO_MASK', 'ANNULUS'),
    ('NO_MASK', 'CENTRAL'),
    ('ANNULUS', 'NO_MASK'),
    ('ANNULUS', 'CENTRAL'),
    ('CENTRAL', 'NO_MASK'),
    ('CENTRAL', 'ANNULUS'),
]
